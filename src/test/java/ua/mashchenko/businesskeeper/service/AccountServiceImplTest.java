package ua.mashchenko.businesskeeper.service;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ua.mashchenko.businesskeeper.domain.Account;
import ua.mashchenko.businesskeeper.domain.BalanceInfo;

import java.math.BigDecimal;

import static org.testng.Assert.*;

/**
 * User: dmashchenko
 * Date: 17.12.12
 * Time: 0:17
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:WEB-INF/spring/main.xml"})
public class AccountServiceImplTest extends AbstractTransactionalTestNGSpringContextTests {
    public AccountService getAccountService() {
        return accountService;
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Autowired
    private AccountService accountService;

    @BeforeMethod
    public void setUp() throws Exception {
        accountService.deleteAll();
        Account account = new Account();
        BalanceInfo balanceInfo = new BalanceInfo();
        balanceInfo.setAmount(BigDecimal.TEN);
        account.setBalanceInfo(balanceInfo);

        accountService.saveBalanceInfo(account);
        accountService.save(account);

        Account account1 = new Account();
        BalanceInfo balanceInfo1 = new BalanceInfo();
        balanceInfo1.setAmount(BigDecimal.ONE);
        account1.setBalanceInfo(balanceInfo1);

        accountService.saveBalanceInfo(account1);
        accountService.save(account1);

    }


    @Test
    public void testFindAll(){
        assertEquals(accountService.findAll().size(), 2);
    }

    @Test
    public void testDeleteAll(){
      accountService.deleteAll();
      assertEquals(true,accountService.findAll().isEmpty());
    }

    @Test
    public void testSave()  {

    }

    @Test
    public void testSaveBalanceInfo() {

    }
}
