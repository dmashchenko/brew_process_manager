define([
    'jquery',
    'countdown',
    'underscore',
    'backbone'
], function ($, countdown, _, Backbone) {

    var Timer = Backbone.Model.extend({


        defaults: {
            el: null,
            startTime: 1 * 5,
            srcAudioEnd: null
        },

        initialize: function () {
            console.log("Timer is started")
        },

        start: function () {

            var that = this;

            var finished = false;

            function callback(event) {
                $this = $(this);
                switch (event.type) {
                    case "seconds":
                    case "minutes":
                    case "hours":
                    case "days":
                    case "weeks":
                    case "daysLeft":
                        $this.find('span#' + event.type).html(event.value);
                        if (finished) {

                            $this.fadeTo(0, 1);
                            finished = false;
                        }
                        break;
                    case "finished":
                        $this.fadeTo('slow', .5);
                        finished = true;
                        new Audio(that.get('srcAudioEnd')).play();
                        break;
                }
            }

            $(this.get("el")).countdown(this.get("startTime"), callback);


        }

    });

    return Timer;

});
