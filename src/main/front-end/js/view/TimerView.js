define([
    'jquery',
    'underscore',
    'backbone',
    'model/Timer',
    'text!template/timer/timer.html'
], function ($, _, Backbone, Timer, timerHtml) {

    var TimerView = Backbone.View.extend({
         className: "timer",


        initialize: function () {


            this.model = new Timer({el: this.el, srcAudioEnd:"tada.wav"});


        },

        render: function () {
            this.$el.html(_.template(timerHtml, { timer: this.model.toJSON() ,timerView : {class : this.className}}));
            this.model.start();
        }

    });

    return TimerView;

});
