define([
    'jquery',
    'underscore',
    'backbone',
    'view/TimerView',
    'text!template/home/app.html'
], function ($, _, Backbone, TimerView, appHtml) {


    var AppView = Backbone.View.extend({
        el: $("#page"),

        render: function () {

            this.$el.html(appHtml);
            var timer = $("<div />");
            $(this.$el).append(timer);
            var timerView = new TimerView({el: timer});

            timerView.render();

        }

    });

    return AppView;

});
