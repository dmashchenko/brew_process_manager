define([
  'jquery',
  'underscore',
  'backbone',
  'model/Footer',
  'text!template/footer/footer.html'
], function($, _, Backbone,Footer, footerHtml){

  var FooterView = Backbone.View.extend({
    el: $("#footer"),

    initialize: function() {

        this.model = new Footer();


    },

    render: function(){

      this.$el.html(_.template( footerHtml, { footer: this.model.toJSON() } ));
    }

  });

  return FooterView;
  
});
