// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'view/home/AppView',
  'view/footer/FooterView'
], function($, _, Backbone, AppView, FooterView) {
  
  var AppRouter = Backbone.Router.extend({
    routes: {
      // Default
      '*actions': 'defaultAction'
    }
  });
  
  var initialize = function(){

    var app_router = new AppRouter;


    app_router.on('route:defaultAction', function (actions) {
     
       // We have no matching route, lets display the home page 
        var appView = new AppView();
        appView.render();

         // unlike the above, we don't call render on this view
        // as it will handle the render call internally after it
        // loads data 
        var footerView = new FooterView();
         footerView.render();

    });

    Backbone.history.start();
  };
  return { 
    initialize: initialize
  };
});
