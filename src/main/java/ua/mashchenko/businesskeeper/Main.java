package ua.mashchenko.businesskeeper;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.bio.SocketConnector;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Hello world!
 */
public class Main {
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {

        log.info("Application init...");
        WebAppContext app = new WebAppContext();
        app.setContextPath("/");
        app.setWar("./target/classes");
        app.setParentLoaderPriority(true);

        Server server = new Server();
        Connector connector = new SocketConnector();
        connector.setPort(8080);
        connector.setMaxIdleTime(60000);
        server.setConnectors(new Connector[]{connector});
        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[]{app});
        server.setHandler(handlers);
        server.setStopAtShutdown(true);

        server.start();
        log.info("Application started...");
        server.join();

    }
}
