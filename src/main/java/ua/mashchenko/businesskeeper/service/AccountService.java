package ua.mashchenko.businesskeeper.service;

import org.springframework.data.repository.CrudRepository;
import ua.mashchenko.businesskeeper.domain.Account;
import ua.mashchenko.businesskeeper.domain.BalanceInfo;
import ua.mashchenko.businesskeeper.dto.AccountDTO;

import java.util.List;

/**
 * User: dmashchenko
 * Date: 16.12.12
 * Time: 11:40
 */
public interface AccountService {

    List<Account> findAll();
    void deleteAll();
    Account save(Account account);
    BalanceInfo saveBalanceInfo(Account account);

    Account finOne(Long id);
}
