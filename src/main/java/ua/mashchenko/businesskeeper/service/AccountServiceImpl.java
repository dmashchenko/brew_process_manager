package ua.mashchenko.businesskeeper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import ua.mashchenko.businesskeeper.domain.Account;
import ua.mashchenko.businesskeeper.domain.BalanceInfo;
import ua.mashchenko.businesskeeper.repository.AccountRepository;
import ua.mashchenko.businesskeeper.repository.BalanceInfoRepository;

import java.util.Collections;
import java.util.List;

/**
 * User: dmashchenko
 * Date: 16.12.12
 * Time: 11:47
 */

@Service("accountService")
@Repository
@Transactional
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private BalanceInfoRepository balanceInfoRepository;

    @Override
    public List<Account> findAll() {
        List<Account> accounts = (List< Account>)accountRepository.findAll();
        return accounts.isEmpty() ? Collections.<Account>emptyList() : accounts;
    }

    @Override
    public void deleteAll() {
        accountRepository.deleteAll();
    }

    @Override
    public Account save(Account account) {
       return accountRepository.save(account);
    }

    @Override
    public BalanceInfo saveBalanceInfo(Account account) {
        Assert.notNull(account);
        Assert.notNull(account.getBalanceInfo(),account +" has no BalanceInfo");
        return balanceInfoRepository.save(account.getBalanceInfo());
    }

    @Override
    public Account finOne(Long id) {
        return accountRepository.findOne(id);
    }


}
