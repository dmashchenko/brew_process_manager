package ua.mashchenko.businesskeeper.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * User: dmashchenko
 * Date: 15.12.12
 * Time: 21:27
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    public BaseEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
           return      "id=" + id;
    }
}
