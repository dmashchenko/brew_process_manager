package ua.mashchenko.businesskeeper.domain;

import javax.persistence.Entity;
import java.math.BigDecimal;

/**
 * User: dmashchenko
 * Date: 15.12.12
 * Time: 22:53
 */
@Entity
public class BalanceInfo extends BaseEntity{
    private BigDecimal amount = BigDecimal.ZERO;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String
    toString() {
        return "BalanceInfo{" +
                "amount=" + amount +
                '}';
    }
}
