package ua.mashchenko.businesskeeper.domain;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.math.BigDecimal;

/**
 * User: dmashchenko
 * Date: 15.12.12
 * Time: 22:23
 */
@Entity
public class Account extends BaseEntity{
    @OneToOne
    private BalanceInfo balanceInfo;

    public Account() {
    }

    public BalanceInfo getBalanceInfo() {
        if(balanceInfo == null) { this.balanceInfo = new BalanceInfo();}
        return balanceInfo;
    }

    public void setBalanceInfo(BalanceInfo balanceInfo) {
        this.balanceInfo = balanceInfo;
    }

    @Override
    public String toString() {
        return "Account{" + super.toString()+
                " balanceInfo=" + balanceInfo +
                "} " ;
    }
}
