package ua.mashchenko.businesskeeper.repository;

import org.springframework.data.repository.CrudRepository;
import ua.mashchenko.businesskeeper.domain.BalanceInfo;

/**
 * User: dmashchenko
 * Date: 16.12.12
 * Time: 13:48
 */
public interface BalanceInfoRepository extends CrudRepository<BalanceInfo,Long> {
}
