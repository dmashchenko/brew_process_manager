package ua.mashchenko.businesskeeper.repository;

import org.springframework.data.repository.CrudRepository;
import ua.mashchenko.businesskeeper.domain.Account;

/**
 * User: dmashchenko
 * Date: 16.12.12
 * Time: 12:02
 */
public interface AccountRepository extends CrudRepository<Account,Long> {
}
