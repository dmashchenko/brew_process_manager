package ua.mashchenko.businesskeeper.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ua.mashchenko.businesskeeper.exception.ApplicationException;

/**
 * User: dmashchenko
 * Date: 25.12.12
 * Time: 1:35
 */
public class ApplicationController {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public  @ResponseBody ApplicationException.RESTResponse defaultHandleException(Exception e){
        return new ApplicationException("Internal Server Error").getResponse();
    }

    @ExceptionHandler(ApplicationException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public  @ResponseBody ApplicationException.RESTResponse defaultHandleException(ApplicationException ae){

        return ae.getResponse();
    }
}
