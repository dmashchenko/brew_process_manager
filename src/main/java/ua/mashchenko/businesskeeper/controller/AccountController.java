package ua.mashchenko.businesskeeper.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ua.mashchenko.businesskeeper.domain.Account;
import ua.mashchenko.businesskeeper.dto.AccountDTO;
import ua.mashchenko.businesskeeper.exception.ApplicationException;
import ua.mashchenko.businesskeeper.repository.AccountRepository;
import ua.mashchenko.businesskeeper.service.AccountService;

import java.util.Arrays;
import java.util.List;

/**
 * User: dmashchenko
 * Date: 23.12.12
 * Time: 1:07
 */

@RequestMapping("/")
@Controller
public class AccountController extends ApplicationController {
    private static final Logger log = LoggerFactory.getLogger(AccountController.class);
    @Autowired
    AccountService accountService;



    @RequestMapping(value = "/init",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String get()throws ApplicationException {
        return "{beepUrl: 'data.wav', intervals: {5,10,15}}";

    }
}
