package ua.mashchenko.businesskeeper.dto;

import org.springframework.util.Assert;
import ua.mashchenko.businesskeeper.domain.Account;
import ua.mashchenko.businesskeeper.domain.BalanceInfo;

import java.math.BigDecimal;

/**
 * User: dmashchenko
 * Date: 24.12.12
 * Time: 23:26
 */
public class AccountDTO extends BaseDTO {
    private BigDecimal balance;

    public AccountDTO(Account account) throws NullPointerException{
        super(account.getId());
        this.balance = account.getBalanceInfo().getAmount();
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
