package ua.mashchenko.businesskeeper.dto;

import java.io.Serializable;

/**
 * User: dmashchenko
 * Date: 24.12.12
 * Time: 23:36
 */
public class BaseDTO implements Serializable {
    private Long id;

    public BaseDTO() {
    }

    public BaseDTO(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
