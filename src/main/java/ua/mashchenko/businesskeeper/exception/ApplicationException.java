package ua.mashchenko.businesskeeper.exception;

import java.io.*;

/**
 * User: dmashchenko
 * Date: 25.12.12
 * Time: 1:38
 */
public class ApplicationException extends Exception {

    private RESTResponse response = null;

    public ApplicationException(String message) {
        super(message);
        this.response = new RESTResponse(message);
    }

    public RESTResponse getResponse() {
        return response;
    }

    public class RESTResponse implements Serializable {
       private String message;

        public RESTResponse() {
        }

        RESTResponse(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
